import 'dart:async';
import 'dart:convert';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scanner_app/models/event.dart';
import 'package:scanner_app/models/verifyResponse.dart';
import 'package:scanner_app/widgets/wide_button.dart';
import 'models/session.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return new MaterialApp(
      home: new HomePage(),
      theme: ThemeData.dark(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {
  String message = "";
  String currentStatus = "";
  Session client = new Session();
  Event selectedEvent;
  List<Event> events = [];
  bool isPosting = false;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => {getEvents()});
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    Widget eventSelect, postLoader, statusIcon;

    if (this.currentStatus == "success") {
      statusIcon = Icon(Icons.done, color: Colors.greenAccent, size: 60);
    } else if (this.currentStatus == "failure") {
      statusIcon = Icon(Icons.close, color: Colors.redAccent, size: 60);
    } else if (this.currentStatus == "loading") {
      statusIcon = new CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation(Colors.deepPurpleAccent),
      );
    } else {
      statusIcon =
          Text("Select an event and scan", style: TextStyle(fontSize: 20));
    }
    if (events.length == 0) {
      eventSelect = new CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation(Colors.deepPurpleAccent),
      );
    } else {
      eventSelect = new DropdownButton<Event>(
        value: selectedEvent,
        icon: Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        style: TextStyle(color: Colors.white),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (Event newValue) {
          setState(() {
            selectedEvent = newValue;
          });
        },
        items: events.map<DropdownMenuItem<Event>>((Event value) {
          return DropdownMenuItem<Event>(
            value: value,
            child: Text(value.name),
          );
        }).toList(),
      );
    }

    return new Scaffold(
        appBar: new AppBar(
          title: new Text('CSI Scanner App'),
          backgroundColor: Colors.deepPurpleAccent,
        ),
        body: new Center(
          child: new Column(
            children: <Widget>[
              SizedBox(height: 20),
              Container(
                  height: screenHeight * 0.3,
                  child: Image.asset('assets/csi_logo.png')),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[new Text('Select Event: '), eventSelect],
              ),
              SizedBox(height: 20),
              new Container(child: buildWideButton(context, "Scan", scan)),
              SizedBox(height: 20),
              new Container(
                  width: 300,
                  child: new Text(message,
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center)),
              SizedBox(height: 20),
              statusIcon
            ],
          ),
        ));
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() {
        this.message = "Marking $barcode";
        isPosting = true;
        this.currentStatus = "loading";

        if (this.selectedEvent == null) {
          this.message = "Please select an event";
          this.currentStatus = "failure";
        } else {
          postAttendance(barcode);
        }
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.message = 'Grant me the fucking permissions, bitch.';
          this.currentStatus = "failure";
        });
      } else {
        setState(() {
          this.message = 'Unknown error: $e';
          this.currentStatus = "failure";
        });
      }
    } on FormatException {
      setState(() {
        this.message = "Don't fuck with me bitch.";
        this.currentStatus = "failure";
      });
    } catch (e) {
      setState(() {
        this.message = 'Unknown error: $e';
        this.currentStatus = "failure";        
        });
    }
  }

  Future login() async {
    var loginUrl = 'https://gravitas.csivit.com/auth/adminLogin';
    await this
        .client
        .post(loginUrl, {'username': 'davidbart', 'password': 'openglsux'});
  }

  Future getEvents() async {
    await this.login();

    var eventsUrl = 'https://gravitas.csivit.com/events';
    var response = await client.get(eventsUrl);

    setState(() {
      var events = json.decode(response.body);
      this.events = [];
      for (final event in events) {
        this.events.add(Event.fromJSON(event));
      }
      this.selectedEvent = this.events[0];
    });
  }

  Future postAttendance(String userid) async {
    String url =
        "https://gravitas.csivit.com/verifyParticipant/${selectedEvent.gravitasID}/$userid";
    var response = await client.get(url);
    VerifyResponse obj;
    try {
      obj = VerifyResponse.fromJSON(json.decode(response.body));
    } catch (e) {
      obj.status = 'failure';
      obj.message = 'ServerError';
    }
    var message;
    if (obj.status == 'failure') {
      if (obj.message == 'ServerError') {
        message = 'Server Error. Ask Rohan to fix his code.';
      } else if (obj.message == 'AlreadyAttended') {
        message = 'Participant $userid already attended';
      } else if (obj.message == 'ParticipantNotFound') {
        message = 'Participant not registered/Not in DB';
      }
    } else if (obj.status == 'success') {
      message = "Attendance posted for $userid";
    }

    setState(() {
      this.isPosting = false;
      this.message = message;
      this.currentStatus = obj.status;
    });
  }
}
