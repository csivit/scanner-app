class Event {
  String name;
  String gravitasID;

  Event({this.name, this.gravitasID});
  factory Event.fromJSON(Map<String, dynamic> json) {
    return Event(
      name: json["name"],
      gravitasID: json["gravitasID"]
    );
  }
}