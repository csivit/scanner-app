class VerifyResponse {
  String status;
  String message;

  VerifyResponse({this.status, this.message});
  factory VerifyResponse.fromJSON(Map<String, dynamic> json) {
    return VerifyResponse(
      status: json["status"],
      message: json["message"]
    );
  }
}