import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget buildWideButton(BuildContext context, String text, onTapCallback) {
  return GestureDetector(
    child: FractionallySizedBox(
      widthFactor: 0.9,
      child: Container(
        height: MediaQuery.of(context).size.height * (7.2 / 100),
        decoration: BoxDecoration(
            color: Colors.deepPurpleAccent),
        child: Center(
            child: Text(text,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold))),
      ),
    ),
    onTap: onTapCallback,
  );
}
